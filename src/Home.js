import Button from '@mui/material/Button';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import {useNavigate} from 'react-router-dom'
import './Home.css'
import {Link} from 'react-router-dom'
function Home() {
    const navigate = useNavigate()


    const theme = createTheme({
        status: {
          danger: '#e53e3e',
        },
        palette: {
          primary: {
            main: '#0971f1',
            darker: '#053e85',
          },
          neutral: {
            main: '#ad916e',
            contrastText: '#fff',
          },
        },
      });
    
    return (
      <div className="home">
          <nav>
      <Link className="link" to="/about">על הקטלוג  </Link>
      <Link className="link" to="/tiks">לתיקי היסוד  </Link>
      
    </nav>
      {/* <p className="ftitle">ברוכים הבאים לקטלוג תיקי יסוד</p> */}
      {/* <ThemeProvider  theme={theme}>
              <Button  onClick={() => navigate('/tiks')}
              color="neutral" className="watch"  variant="outlined">לתיקי היסוד</Button>
          </ThemeProvider> */}
      </div>
    );
  }
  
  export default Home;
  