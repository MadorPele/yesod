import {Link} from 'react-router-dom'
import './Tiks.css'
import { useCollectionData } from 'react-firebase-hooks/firestore';
import {db} from './firebase/config'
import { collection } from 'firebase/firestore';
import { useNavigate} from 'react-router-dom'
import TiksList from './TiksList';

const tiks = ['חילוץ', 'מפקדים', 'השתלמות','לוחמים', 'לוגיסטיקה', 'כיבוי','מיגון', 'תקשוב', 'רובאות', 'מודיעין' ,'צמ"ה', 'אנו"ח', 'רפואה','אכלוסייה הכשרות אזרחיות', 'אוכלוסייה', 'אג"ם', 'אב"ך' ,'אב"ך צה"ל' ]
function Tiks() {
  const query = collection(db, 'tiks')
  const  [docs, loading, error] =  useCollectionData(query)
  const navigate = useNavigate()


  const handleJewelryClick = (id) => {
      navigate(`/tiks/${id}`);
  }

  return (
    <div className="tiks">
      {/* <nav>
      <Link className="link ab" to="/about">על הקטלוג  </Link>
      </nav>
    <h1 style={{textAlign: 'center', position:'absolute', bottom:'80vh', right: '48vw'}}>התיקים</h1>
    <div className='container'>
    {tiks.map((tik, index) => (
      <div className='item' key={index}>{tik}</div>
    ))}</div> */}

<h1 style={{textAlign: 'center', position:'absolute', bottom:'85vh', right: '48vw'}}>התיקים</h1>
      {loading && <p>טוען...</p>}
      <div  className='container'>
      {docs && docs.map(doc => 
        <div key={doc.name} className='item' onClick={() => handleJewelryClick(doc.name)}>{doc.name}</div>
      )}
      </div>
    </div>

  );
}

export default Tiks;
