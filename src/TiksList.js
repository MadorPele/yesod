import { useCollectionData } from 'react-firebase-hooks/firestore';
import {db} from './firebase/config'
import { collection } from 'firebase/firestore';
import {useParams, Route, Routes, useNavigate} from 'react-router-dom'
import './TiksList.css'
import im from './images/try.JPG'

 function TiksList (){
    const { id } = useParams()
    const path = `tiks/${id}/sub`
    const query = collection(db, path)
    const  [docs, loading, error] =  useCollectionData(query)
    const navigate = useNavigate()

    const back = () => {
        navigate('/tiks')
    }

    return (
        <div className='tiks'>
            <p className='back' onClick={back}>חזור</p>
            <div className='list'>
            {docs && docs.map(doc => 
                <div key={doc.name} className='ol'>{doc.name}</div>
            )}
            </div>
        </div>
        // <div>
        //     <img src={im} alt="i" className='n' />
        // </div>
    );


}
export default TiksList;