import { initializeApp } from "firebase/app";
import {getFirestore} from 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyDv6TNq1TN0w489dZx1iTIemQpS9VW8yjU",
  authDomain: "yesod-5c3ed.firebaseapp.com",
  projectId: "yesod-5c3ed",
  storageBucket: "yesod-5c3ed.appspot.com",
  messagingSenderId: "253235028624",
  appId: "1:253235028624:web:78be7ae486a85613402e15"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app)

export { db }