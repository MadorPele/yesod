import './App.css';
import { Route, Routes, BrowserRouter, Link, Navigate, useNavigate} from 'react-router-dom'
import Tiks from './Tiks'
import Home from './Home'
import About from './About'
import { useEffect } from 'react';
import pik from './images/pik.png'
import TiksList from './TiksList';



function App() {
  
const navigate = useNavigate()
  
  
  useEffect(() => {
    navigate('/home')
  }, [])

  const backHo = () => {
    navigate('/home')
  }
  return (


  <div className="app">
  <img className="pikud" src={pik} alt="pikud" onClick={backHo} />
    <Routes>
      <Route  path="/tiks" element={<Tiks />} />
      <Route  path="/home" element={<Home />} />
      <Route  path="/about" element={<About />} />
      <Route path="/tiks/:id" element={<TiksList />} />
    </Routes>
  </div>
  );
}

export default App;
